# DownStream

DownStream is a simple GUI for youtube-dl, for desktop and mobile Linux

![downstream screenshot](doc/downstream1.png "default use-case: paste a URL")
![downstream screenshot](doc/downstream2.png "convenience function: auto-pasted URL")

## Features
* auto-pastes URL from the clipboard on startup
* allows quality selection
* allows to edit the title (pre-filled, if URL in clipboard)
* Wayland aware (clipboard)
* success notifications
* Localizations: en, de
* small footprint (5 KiB so far ...)

## Installation
    sudo apt install yad youtube-dl
    sudo apt install libnotify-bin  wl-clipboard  # optional / recommended for finish notifications and url auto-pasting on wayland

    wget https://codeberg.org/caliga/DownStream/raw/branch/main/DownStream.desktop
    wget https://codeberg.org/caliga/DownStream/raw/branch/main/downstream
    sed -i "s/purism/$USER/g" DownStream.desktop    # only if your user is NOT purism
    chmod +x downstream
    mkdir ~/.local/bin/
    mv DownStream.desktop ~/.local/share/applications/
    mv downstream ~/.local/bin/


## Background
A quick search did not turn up usable, working (non-abandoned) UIs for youtube-dl.
Inspired by a series of articles on 'Easy Librem 5 App Development' ([screenshot](https://puri.sm/posts/easy-librem-5-app-development-take-a-screenshot/), [flashlight](https://puri.sm/posts/easy-librem-5-app-development-flashlight/), [scaling](https://puri.sm/posts/easy-librem-5-app-development-scale-the-screen/)) I decided to write a very simple [yad](https://www.mankier.com/1/yad)-based app.

## Bugs
* auto-pasting in Wayland is untested
* screen-fitting on mobile Linux ([Librem 5](https://puri.sm/products/librem-5/) or [PinePhone](https://www.pine64.org/pinephone/)) is untested

## Improvements (TODO)
* add more Localizations
* Maybe show some spinner while waiting for the title to be fetched (dialog is shown delayed if a URL is processed)
* proper packaging
