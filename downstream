#!/bin/bash

declare -rg DOWNLOAD_PATH="$HOME/Downloads/Video"
declare -rg ICON=cloud-download
declare -rg FILENAME_REPLACE=$'\\/:|'
declare -rg REPLACE_WITH='_'

declare -Ag TL

function translations()
{
    local -r locale=${LANG:0:2}
    case "$locale" in
        ( de ) translate_de ;;
       # ( fr ) translate_fr ;; # TODO example how to add translations
    esac

   translate_en
   # translate_fr
}

function translate_en()
{
    tl error        'DownStream error:'
    tl yad          'Please install the yad package'
    tl ytdl         'Please install the youtube-dl package'
    tl notify       'notify-send required to show messages. Please install the libnotify-bin package'
    tl paste        'Paste a video URL'
    tl example      'e.g.'
    tl emptyTitle   'Leave title empty for automatic naming.'
    tl autoHint     'Hint: For auto-pasting, copy URL before starting DownStream.'
    tl dlVideo      'Download video (pasted from clipboard):'
    tl noTitle      '(could not fetch title. Download might fail)'
    tl destDir      'Downloading to:'
    tl bestQual     'Best'
    tl audio        'Audio only'
    tl url          'URL'
    tl title        'Title'
    tl prefQual     'Preferred quality'
    tl changeDir    'Could not open directory:'
    tl success      'Download successful'
    tl fail         'Download failed'
}


function translate_de()
{
    tl error        'DownStream Fehler:'
    tl yad          'Bitte das yad Paket installieren'
    tl ytdl         'Bitte das youtube-dl Paket installieren'
    tl notify       'notify-send wird benötigt um Benachrichtigungen anzuzeigen. Bitte das libnotify-bin Paket installieren'
    tl paste        'Video URL einfügen'
    tl example      'z.B.'
    tl emptyTitle   'Titel leer lassen für automatische Benennung'
    tl autoHint     'Tipp: Für automatisches Einfügen die URL vor dem Start von DownStream kopieren'
    tl dlVideo      'Video herunterladen (aus Zwischenablage eingefügt):'
    tl noTitle      '(Titel nicht verfügbar. Download könnte fehlschlagen)'
    tl destDir      'Zielpfad:'
    tl bestQual     'Höchste Qualität'
    tl audio        'Nur Ton'
    # url
    tl title        'Title'
    tl prefQual     'Bevorzugte Qualität'
    tl changeDir    'Konnte Verzeichnis nicht öffnen:'
    tl success      'Download erfolgreich'
    tl fail         'Download fehlgeschlagen'
}

function tl()
{
    [ -n "$1" ] || return
    [ -n "$2" ] || return
    [ -n "${TL[$1]}" ] && return
    TL[$1]="$2"
}

function die()
{
    echo "$1"
    exists notify-send && notify-send "$(echo -e "${TL[error]}\n$1")"
    exit 1
}

function notify()
{
    exists notify-send && notify-send "$(echo -e "$1")"
}

function exists()
{
    command -v "$1" &> /dev/null
}

function clipboard()
{
    local clip=
    exists wl-paste && clip="$(wl-paste --no-newline 2> /dev/null)"
    [ -z "$clip" ] && exists xclip && clip="$(xclip -out -selection clipboard 2> /dev/null)"
    echo "$clip"
}

function main()
{
    translations

    exists yad || die "${TL[yad]}"
    exists youtube-dl || die "${TL[ytdl]}"
    exists notify-send || echo "${TL[notify]}"

    local url="$(clipboard)"
    [[ "$url" == http* ]] || url=""
    echo "Clipboard '$url'"
    
    local text="${TL[paste]}\n${TL[example]} https://www.youtube.com/watch?v=dQw4w9WgXcQ\n${TL[emptyTitle]}\n\n${TL[autoHint]}\n"
    local title=''
    local urlRO=''
    local focus=1

    if [ -n "$url" ]; then
        title="$(youtube-dl --get-title "$url")"
        
        text="${TL[dlVideo]}\n"
        [ -z "$title" ] && text+="${TL[noTitle]}\n"
        [ -n "$title" ] && urlRO=':RO'
        [ -n "$title" ] && focus='2'
    else
        url='https://'
    fi
    echo "Title (fetched) '$title'"

    text+="\n${TL[destDir]} $DOWNLOAD_PATH\n"

    local input= # separate declaration to get proper yad exit code
    input="$(echo -e "$url\n$title\n${TL[bestQual]}!1080p!^720p!480p!${TL[audio]} (auto)!${TL[audio]} (mp3)!${TL[audio]} (vorbis)" | yad --title 'DownStream' \
        --window-icon=$ICON \
        --text "$text" \
        --form \
        --separator=$'\n' \
        --field="${TL[url]}$urlRO" \
        --field="${TL[title]}" \
        --field="${TL[prefQual]}":CB \
        --focus-field=$focus \
        )"
    [ $? -eq 0 ] || exit # quit on Cancel

    url="$(echo "$input" | cut --delimiter=$'\n' --fields=1)"
    title="$(echo "$input" | cut --delimiter=$'\n' --fields=2)"
    local -r quality="$(echo "$input" | cut --delimiter=$'\n' --fields=3)"

    local needle=
    for (( i=0; i<${#FILENAME_REPLACE}; i++ )); do
        needle="${FILENAME_REPLACE:$i:1}"
        title="${title//"$needle"/$REPLACE_WITH}"
        echo "$needle - $title"
    done
    echo "Title (edited) '$title'"

    local mode='--format'
    local format='best'
    case "$quality" in
        (    1080p ) format='[height<=1080]' ;;
        (     720p ) format='[height<=720]' ;;
        (     480p ) format='[height<=480]' ;;
        (   *auto* ) mode='--extract-audio --audio-format'; format='best' ;;
        (    *mp3* ) mode='--extract-audio --audio-format'; format='mp3' ;;
        ( *vorbis* ) mode='--extract-audio --audio-format'; format='vorbis' ;;
    esac

    local filename='%(title)s.%(ext)s'
    [ -n "$title" ] && filename="$title.%(ext)s"

    mkdir -p "$DOWNLOAD_PATH"
    cd "$DOWNLOAD_PATH" || die "${TL[changeDir]} '$DOWNLOAD_PATH'"

    local result="${TL[fail]}\n$title\n$url"

    youtube-dl $mode $format --output "$filename" "$url"

    [ $? -eq 0 ] && result="${TL[success]}\n$title\n$url to $DOWNLOAD_PATH"
    notify "$result"
}

main
